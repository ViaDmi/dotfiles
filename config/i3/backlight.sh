#!/bin/sh
set -e
file="/sys/class/backlight/intel_backlight/brightness"
current="$(cat "$file")"
new="$current"

[ "$1" = "-inc" ] && new=$(( current + $2 ))
[ "$1" = "-dec" ] && new=$(( current - $2 ))
echo "$new" | tee "$file"
