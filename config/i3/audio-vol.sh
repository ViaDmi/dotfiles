#!/bin/sh
#set -e
#path="/sys/class/backlight/intel_backlight"
#max="$(cat "$path/max_brightness")"
#cur="$(printf "%2d" "$(echo "$(cat "$path/brightness") * 100 / $max" | bc)")"
pactl list sinks | grep -e 'Volume:' -e 'Name:'

prn="$(dmenu -fn "hack-16" -sb '#cc7b20' -nb '#1d2021' -sf '#282828' -nf '#ebdbb2' -c -bw 4 -W 440 -p "Volume: $cur% →" < /dev/null)"


# sometimes need `pulseaudio -k`
# validate input here
# pactl set-sink-volume @DEFAULT_SINK@ "$prn"%

# Validate input: must be an integer between 0 and 100
#if [ -z "$1" ] || ! [ "$1" -ge 0 ] 2>/dev/null || [ "$1" -gt 100 ]; then
#    echo "Usage: $0 <volume> (0-100)"
#    exit 1
#fi
#
## Set volume with pactl (for PulseAudio)
#pactl set-sink-volume @DEFAULT_SINK@ "$1%"
