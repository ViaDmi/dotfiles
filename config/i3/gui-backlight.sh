#!/bin/sh
# see https://wiki.archlinux.org/title/Backlight for permission troubleshooting
set -e
path="/sys/class/backlight/intel_backlight"
max="$(cat "$path/max_brightness")"
cur="$(printf "%2d" "$(echo "$(cat "$path/brightness") * 100 / $max" | bc)")"
prn="$(dmenu -fn "hack-16" -sb '#cc7b20' -nb '#1d2021' -sf '#282828' -nf '#ebdbb2' -c -bw 4 -W 440 -p "Brightness: $cur% →" < /dev/null)"
# validate input here
new="$(echo "$prn * $max / 100" | bc)"
echo "$new" > "$path/brightness"
