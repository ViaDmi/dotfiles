#!/bin/sh
choice="$(printf " Poweroff\n Reboot\n Logout" | dmenu -c -i -l 3 -bw 6 -W 400 -fn "hack-16" -sb '#cc7b20' -nb '#1d2021' -sf '#282828' -nf '#ebdbb2')"
case $choice in
    " Poweroff") loginctl poweroff ;;
    " Reboot")   loginctl reboot ;;
    " Logout")   i3-msg exit ;;
    *) exit 1 ;;
esac
