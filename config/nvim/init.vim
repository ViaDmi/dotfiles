""" nvim config file

""" basic config
" common
set encoding=utf-8
set fileformat=unix
set mouse=a               " enable mouse
set clipboard=unnamedplus " use system clipboard

" set lines numbering
set number
set relativenumber

" tabs settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab

" indent settings
set autoindent
set smartindent

" basic settings for different files
filetype plugin on
filetype indent on

" make tab visiable
set list
set listchars=tab:␉·

" set 24 bit colors
if has('termguicolors')
    set termguicolors
endif

"" no so good as I expected
" set scrolloff=7
" set colorcolumn=120
" inoremap jk <esc>

"" trying solve language layout issue (not best)
"set nolangremap
"set keymap=russian-jcukenwin
"set spelllang=ru_yo,en_us
"" enlish by default
"set iminsert=0 " insert mode
"set imsearch=0 " search

" set leader
map <Space> <Leader>

" switch tabs by number like in browser
noremap <M-1> 1gt
noremap <M-2> 2gt
noremap <M-3> 3gt
noremap <M-4> 4gt
noremap <M-5> 5gt
noremap <M-6> 6gt
noremap <M-7> 7gt
noremap <M-8> 8gt
noremap <M-9> 9gt

" return to last edit position when opening files 
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" basic folds settings
set foldmethod=syntax
set foldnestmax=1
" open folds by default
autocmd BufWinEnter * silent! :%foldopen!
"set foldlevel=99 " open all fold by default

" remember folds
augroup remember_folds
  autocmd!
  autocmd BufWinLeave ?* mkview
  autocmd BufWinEnter ?* silent! loadview
augroup END

" automatically deletes all trailing whitespace and newlines at end of file on save
" autocmd BufWritePre * %s/\s\+$//e
" autocmd BufWritePre * %s/\n\+\%$//e

" Map Ctrl-Backspace to delete the previous word in insert mode.
imap <C-BS> <C-W>

" select and copy all file by Leader-a
map <Leader>a :%y+<CR>

""" Plugins
call plug#begin()
    " appearance
    Plug 'vim-airline/vim-airline'
    Plug 'rafi/awesome-vim-colorschemes'
    Plug 'ap/vim-css-color'
    " utilities
    Plug 'lervag/vimtex'
    Plug 'bfrg/vim-cpp-modern'
    Plug 'vim-python/python-syntax'
    " Telescope
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.5' }
    " LSP
    " Plug 'prabirshrestha/vim-lsp'
    " Plug 'mattn/vim-lsp-settings'
    " Plug 'prabirshrestha/asyncomplete.vim'
    " Plug 'prabirshrestha/asyncomplete-lsp.vim'
call plug#end()

colorscheme gruvbox

" find files using Telescope command-line sugar
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" netrw file explorer settings
let g:netrw_banner       = 0 " hide banner above files
let g:netrw_liststyle    = 3 " tree instead of plain view
let g:netrw_browse_split = 3 " vertical split window when Enter pressed on file

" airline settings
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.colnr     = ' C:'
let g:airline_symbols.linenr    = ' L:'
let g:airline_symbols.maxlinenr = '☰ '
" let g:airline#extensions#tabline#enabled = 1

""" python-syntax settings
let g:python_highlight_all = 1

""" vimtex settings
let g:vimtex_view_general_viewer = 'zathura'
" without following vim can be slow cause extra complex regex for highlighting tex files
let g:vimtex_matchparen_enabled = 0
let g:vimtex_motion_matchparen = 0

let g:Tex_CompileRule_pdf = 'xelatex --interaction=nonstopmode --shell-escape $*'
"let g:vimtex_compiler_latexmk = { 
"        \ 'executable' : 'latexmk',
"        \ 'options' : [ 
"        \   '-xelatex',
"        \   '-file-line-error',
"        \   '-synctex=1',
"        \   '-interaction=nonstopmode',
"        \ ],
"        \}
