#!/bin/sh

# src: https://github.com/Gustash/sway-screenshot
# changes:
# 1. POSIX
# 2. ShellCheck fixes
# 3. Simplifies for my personal needs

#set -e
fail() { printf "\033[1;31mError:\033[0m %b.\n" "$1" >&2; exit 1; }

usage() {
    cat <<EOF
Usage: sway-screenshot [options ..] -m [mode] -- [command]
sway-screenshot is an utility to easily take screenshot in swaywm using your mouse.

Options:
  -h        show help message
  -m        one of: output, window, region
  -c        copy screenshot to clipboard and don't save image in disk

Modes:
  output    take screenshot of an entire monitor
  window    take screenshot of an open window (disabled)
  region    take screenshot of selected region
EOF
}

save_geometry() {
    if [ "$clipboard" -eq 0 ]; then
        grim -g "${1}" "$save_fullpath"
        #output="$save_fullpath"
        # trim transparent pixels, in case the window was floating and partially, outside the monitor
        # magick convert "$output" -trim +repage "$output"
        #wl-copy < "$output"
    else
        #tmpima="$(mktemp)"
        #trap 'rm -rf -- "$tmpima"' EXIT HUP INT QUIT TERM #PWR
        #mkfifo "$tmpima"
        #grim -g "${1}" > "$tmpima" & # - | magick convert - -trim +repage - > "$tmpima" &
        #wl-copy < "$tmpima"
        #rm -f -- "$tmpima"
        grim -g "${1}" - | wl-copy
    fi
}

begin_grab() {
    case "$1" in
        output) geometry="$(grab_output)" ;;
        region) geometry="$(grab_region)" ;;
#        window) geometry="$(grab_window)" ;;
             *) fail "wrong mode: $1" ;;
    esac
    save_geometry "${geometry}"
}

grab_output() { slurp -or; }

grab_region() { slurp -d; }

#grab_window() {
#    clients="$(swaymsg -t get_tree | jq -r '[.. | ((.nodes? // empty) + (.floating_nodes? // empty))[] | select(.visible and .pid)]')"
#    # Generate boxes for each visible window and send that to slurp; through stdin
#    boxes="$(echo "$clients" | jq -r '.[] | "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height) \(.name)"')"
#    slurp -r <<< "$boxes"
#}

clipboard=0
while getopts "hcm:" opt; do case $opt in
     h) usage; exit 0 ;;
     c) clipboard=1 ;;
     m) mode="$OPTARG" ;;
    \?) usage; fail "Invalid option: -$OPTARG" ;;
     :) fail "option -$OPTARG requires an argument" ;;
esac done
shift $((OPTIND-1))

filename="$(date +'scr-%Y-%m-%d-T%H-%M-%S-%N.png')"
save_fullpath="$HOME/tmp/screen/$filename"
begin_grab "$mode"
