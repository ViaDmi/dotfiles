#!/bin/sh

fail() { printf '%b' "\033[1;31mError:\033[0m $1.\n" >&2; exit 1; }

if [ -n "$1" ]; then
    monitor="$1"
else
    monitor="$(printf "in\ntv\n2k" | dmenu -p " Monitor" -c -i -l 1 -g 3 -bw 6 -W 440 -fn "hack-16" -sb '#cc7b20' -nb '#1d2021' -sf '#282828' -nf '#ebdbb2')"
fi

case "$monitor" in
  "in") args="--primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1 --off" ;;
  "tv") args="--off --output HDMI-1 --primary --mode 1366x768  --pos 0x0 --rotate normal" ;;
  "2k") args="--off --output HDMI-1 --primary --mode 2560x1440 --pos 0x0 --rotate normal" ;;
     *) fail "invalid argument, use 'in', 'tv', '2k'" ;;
esac

# shellcheck disable=SC2086
xrandr --output eDP-1 $args --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off
xwallpaper --zoom ~/.local/share/wallpapers/SG-2k.png
# set audio
# pacmd "set-default-sink alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__Headphones__sink"
