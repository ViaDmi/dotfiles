#!/bin/sh

fail() { printf '%b' "\033[1;31mError:\033[0m $1.\n" >&2; exit 1; }

if [ -n "$1" ]; then
    audio="$1"
else
    audio="$(pacmd list-sinks | grep -e 'name:' -e 'index:' | sed -e 'N;s/\n/ /;s/\tname: //' | cut -c 3- |
             dmenu -p "♪ Audio" -c -i -l 10 -bw 6 -fn "hack-16" -sb '#cc7b20' -nb '#1d2021' -sf '#282828' -nf '#ebdbb2' |
             sed 's/. index: [0-9]* <\(.*\)>$/\1/')" || exit 1
fi

pacmd "set-default-sink $audio"

#case "$audio" in
#  "in") args="--primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1 --off" ;;
#     *) fail "invalid argument, use variants from list" ;;
#esac
