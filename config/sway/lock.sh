#!/bin/sh
# lock screen displaying this image
i3lock --show-keyboard-layout --color=1d2021
# turn the screen off after a delay
sleep 15; pgrep i3lock && xset dpms force off
