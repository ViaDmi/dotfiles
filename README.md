# dotfiles

>Everything you need to know\
>You can see below

![stuff](images/coonfiger.webp)

# stuff
Designed for [Artix Linux](https://artixlinux.org).
Repository also stores [some browser extension settings](browser-ext) and [some system configs](system).
They are simply stored, without deployment.

Usual dotfiles currently managed using the [toml-bombadil](https://github.com/oknozor/toml-bombadil).
However, `bombadil` does not work well (parse error) with [lf config](config/lf/lfrc) due to the presence of `{{ }}`
(`lf`: function, `bombadil`: accesses variable).
Maybe someday I will use another tool.
By the way, I can’t say that I like any of [well-known dotfiles managers](https://wiki.archlinux.org/title/Dotfiles#Tools).
